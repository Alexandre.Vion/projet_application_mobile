package com.example.uapv1705388.emploie_du_temps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

public class UEActivity extends AppCompatActivity{
    public static final String PREFS = "Promo&Group";

    CourseDbHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ue);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("");

        dbh = new CourseDbHelper(getApplicationContext());
        ArrayList<String []> ue = new ArrayList<String[]>();
        ArrayList<String> ueName = dbh.getAllUE();
        for (int i =0; i<ueName.size();i++)
        {
            ue.add(dbh.getUE(ueName.get(i)));
        }
        SharedPreferences settings = getSharedPreferences(PREFS, 0);


        TextView allue = (TextView) findViewById(R.id.allue);
        String allUE ="";

        for (int i =0;i<ue.size();i++)
        {
            allUE += ueName.get(i) + " Temps total : " + ue.get(i)[0] + "\n" + "Temps restant : " + ue.get(i)[1] + "\n";
        }
        allue.setText(allUE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {

            Intent intent =  new Intent(UEActivity.this, HomeActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.schedule)
        {
            return true;
        }
        else if(id == R.id.evaluation)
        {
            return true;
        }
        else if(id == R.id.free_room)
        {
            return true;
        }
        else if(id == R.id.option)
        {
            Intent intent =  new Intent(UEActivity.this, NoSharedPrefHomeActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


    }
}
