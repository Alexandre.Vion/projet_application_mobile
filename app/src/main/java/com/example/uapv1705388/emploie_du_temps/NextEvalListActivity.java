package com.example.uapv1705388.emploie_du_temps;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

public class NextEvalListActivity extends AppCompatActivity {

    private ArrayList<String> starttimes = new ArrayList<String>();
    private ArrayList<String> endtimes = new ArrayList<String>();
    private ArrayList<String> courses = new ArrayList<String>();
    private ArrayList<String> dates = new ArrayList<String>();
    CourseDbHelper dbh;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nextevallist);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("");
        dbh = new CourseDbHelper(getApplicationContext());

        initViewParameter();
        initRecyclerView();


    }

    private void initViewParameter()
    {
        ArrayList<Course> cCourse = dbh.getAllEval(starttimes.size());

        for(int i =0; i<cCourse.size();i++)
        {
            starttimes.add(cCourse.get(i).startTimeToTxt());
            endtimes.add(cCourse.get(i).endTimeToTxt());
            courses.add(cCourse.get(i).courseToTxt());
            dates.add(cCourse.get(i).dateToTxT());
        }
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerViewAdapterCourse adapter = new RecyclerViewAdapterCourse(starttimes,endtimes,courses,dates,this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
