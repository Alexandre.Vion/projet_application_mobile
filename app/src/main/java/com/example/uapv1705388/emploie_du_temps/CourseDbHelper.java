package com.example.uapv1705388.emploie_du_temps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CourseDbHelper extends SQLiteOpenHelper {
    private static final String TAG = CourseDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "cour.db";

    public static final String TABLE_NAME = "cour";

    public static final String _ID = "_id";
    public static final String COLUMN_UE = "UE";
    public static final String COLUMN_START = "HStart";
    public static final String COLUMN_END = "HEnd";
    public static final String COLUMN_LOCATION = "Location";
    public static final String COLUMN_ENSEIGNANT = "enseignant";
    public static final String COLUMN_GROUPE = "groupe";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_DATE = "Date";
    public static final String COLUMN_GPREF = "Gpref";

    public CourseDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +

                COLUMN_UE + " TEXT , " +
                COLUMN_DATE + " INTEGER , " +
                COLUMN_START + " INTEGER, " +
                COLUMN_END+ " INTEGER, " +
                COLUMN_LOCATION+ " TEXT, " +
                COLUMN_ENSEIGNANT+ " TEXT, " +
                COLUMN_GROUPE+ " TEXT, " +
                COLUMN_TYPE+ " TEXT, " +
                COLUMN_GPREF+ " TEXT );";

        db.execSQL(SQL_CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addCourse(Course course) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Inserting Row
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_UE , course.ue);
        cv.put(COLUMN_START, course.hstart);
        cv.put(COLUMN_END, course.hend);
        cv.put(COLUMN_LOCATION, course.location);
        cv.put(COLUMN_TYPE, course.type);
        cv.put(COLUMN_ENSEIGNANT, course.teacher);
        cv.put(COLUMN_GROUPE, course.group);
        cv.put(COLUMN_DATE,course.date);
        cv.put(COLUMN_GPREF,course.grouppref);
        long rowID = 0;
        db.insert(TABLE_NAME,null, cv);
        db.close(); // Closing database connection


        return (rowID != -1);
    }

    public int updateCourse(Course course) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_UE , course.ue);
        cv.put(COLUMN_START, course.hstart);
        cv.put(COLUMN_END, course.hend);
        cv.put(COLUMN_LOCATION, course.location);
        cv.put(COLUMN_TYPE, course.type);
        cv.put(COLUMN_ENSEIGNANT, course.teacher);
        cv.put(COLUMN_GROUPE, course.group);
        cv.put(COLUMN_DATE,course.date);
        cv.put(COLUMN_GPREF,course.grouppref);

        // updating row
        return db.updateWithOnConflict(TABLE_NAME, cv, _ID + " = ?",
                new String[] { String.valueOf(course.id) }, SQLiteDatabase.CONFLICT_IGNORE);
    }


    public ArrayList<Course> getNextCourse()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Course cour = new Course();
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
        String format = s.format(new Date());
        int d = Integer.valueOf(format);
        s.applyPattern("hhmmss");
        format = s.format(new Date());
        int h = Integer.valueOf(format);
        Log.d("course",String.valueOf(h));
        String SELECT_QUERY =
                String.format("SELECT * FROM %s WHERE %s = '2-L3IN' AND ((%s == %s AND %s + 1500 >= %s) OR (%s > %s)) AND ((%s LIKE '%s') OR (%s = '%s')) ORDER BY %s,%s  ASC LIMIT 2",
                        TABLE_NAME,
                        COLUMN_GPREF,
                        COLUMN_DATE,
                        d,
                        COLUMN_START,
                        h,
                        COLUMN_DATE,
                        d,
                        COLUMN_GROUPE,
                        "%L3INFO_TD1%",
                        COLUMN_GROUPE,
                        " L3 INFORMATIQUE",
                        COLUMN_DATE,
                        COLUMN_START
                );
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        cursor.moveToFirst();
        ArrayList<Course> lcourse = new ArrayList<Course>();
        if (cursor.getCount() != 0) {
            lcourse.add(cursorToCourse(cursor));
            while (cursor.moveToNext()) {
                lcourse.add(cursorToCourse(cursor));
            }
        }
        return lcourse;
    }

    public ArrayList<Course> getAllCourDay(int d)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Course> courD = new ArrayList<Course>();
        if (d == 0) {
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
            String format = s.format(new Date());
            d = Integer.valueOf(format);
        }
        String SELECT_QUERY =
                String.format("SELECT * FROM %s WHERE %s = '2-L3IN' AND %s = %s  AND ((%s LIKE '%s') OR (%s = '%s')) ORDER BY %s ASC",
                        TABLE_NAME,
                        COLUMN_GPREF,
                        COLUMN_DATE,
                        d,
                        COLUMN_GROUPE,
                        "%L3INFO_TD1%",
                        COLUMN_GROUPE,
                        " L3 INFORMATIQUE",
                        COLUMN_START
                );
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        cursor.moveToFirst();
        if (cursor.getCount() != 0) {
            courD.add(cursorToCourse(cursor));
            while (cursor.moveToNext()) {
                courD.add(cursorToCourse(cursor));
            }
        }
        return courD;
    }

    public ArrayList<Course> getAllEval(int pos)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Course> courD = new ArrayList<Course>();
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
        String format = s.format(new Date());
        int d = Integer.valueOf(format);
        String SELECT_QUERY =
                String.format("SELECT * FROM %s WHERE %s = '2-L3IN' AND %s = 'Evaluation' AND %s >= %s AND ((%s LIKE '%s') OR (%s = '%s')) ORDER BY %s,%s ASC LIMIT %s ,10",
                        TABLE_NAME,
                        COLUMN_GPREF,
                        COLUMN_TYPE,
                        COLUMN_DATE,
                        d,
                        COLUMN_GROUPE,
                        "%L3INFO_TD1%",
                        COLUMN_GROUPE,
                        " L3 INFORMATIQUE",
                        COLUMN_DATE,
                        COLUMN_START,
                        pos
                );
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        cursor.moveToFirst();
        if (cursor.getCount() != 0) {
            courD.add(cursorToCourse(cursor));
            while (cursor.moveToNext()) {
                courD.add(cursorToCourse(cursor));
            }
        }
        return courD;
    }

public ArrayList<String> getAllUE()
{
    SQLiteDatabase db = this.getReadableDatabase();
    ArrayList<String> allue = new ArrayList<String>();
    String SELECT_QUERY =
            String.format("SELECT DISTINCT  %s FROM %s",
                    COLUMN_UE,
                    TABLE_NAME
            );
    Cursor cursor = db.rawQuery(SELECT_QUERY, null);
    cursor.moveToFirst();
    if (cursor.getCount() > 0) {
        allue.add(cursor.getString(cursor.getColumnIndex(COLUMN_UE)));
        while (cursor.moveToNext()) {
            allue.add(cursor.getString(cursor.getColumnIndex(COLUMN_UE)));
        }
    }

    return allue;
}


    public String[] getUE(String ue)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String SELECT_QUERY =
                String.format("SELECT * FROM %s WHERE %s = '%s' AND ((%s LIKE '%s') OR (%s = '%s'))",
                        TABLE_NAME,
                        COLUMN_UE,
                        ue,
                        COLUMN_GROUPE,
                        "%L3INFO_TD1%",
                        COLUMN_GROUPE,
                        " L3 INFORMATIQUE"
                );
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        cursor.moveToFirst();
        float res =0;
        float all =0;
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
        String format = s.format(new Date());
        int d = Integer.valueOf(format);
        if (cursor.getCount() != 0) {
            all += (cursor.getInt(cursor.getColumnIndex(COLUMN_END))-cursor.getInt(cursor.getColumnIndex(COLUMN_START)))/1000;
            while (cursor.moveToNext())
            {
                all += (cursor.getInt(cursor.getColumnIndex(COLUMN_END))-cursor.getInt(cursor.getColumnIndex(COLUMN_START)))/1000;
                if (d < cursor.getInt(cursor.getColumnIndex(COLUMN_DATE)))
                {
                    res += (cursor.getInt(cursor.getColumnIndex(COLUMN_END))-cursor.getInt(cursor.getColumnIndex(COLUMN_START)))/1000;
                }
            }
        }
        String [] ueh = new String[] {String.valueOf(all/10).replace('.','h'),String.valueOf(res/10).replace('.','h')};
        return ueh;
    }

    public ArrayList<String> getFreeRoom(int date, int hs, int he)
    {
        String r = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String [] room = new String[] {"Amphi Ada","Amphi Blaise","Stat 1 = Info - C 137","Stat 2 = Info - C 136",
                "Stat 3 = Info - C 135","Stat 4 = Info - C 131","Stat 5 = Info - C 130","Stat 6 = Info - C 129",
                "Stat 7 = Info - C 128 (Windows)","Stat 8 = Info - C 127 (Windows)","Stat 9 = Info - C 026",
                "S1 = C 042 Nodes","S2 = C 040","S2 BIS = C 038","S3 = C 036","S4 = C 034","S5 = C 024","S6 = C 022",
                "S7 = C 032", "S8 = C 030","ELEC = C 133 Electronique","CISCO = C 133 Réseaux","RES = C 132 Reseaux",
                "ROB = C 134 Robotique"};
        ArrayList<String> freeRoom = new ArrayList<String>();
        for (int i =0; i< room.length;i++)
        {
            freeRoom.add(room[i]);
        }
        String SELECT_QUERY =
                String.format("SELECT * FROM %s WHERE %s = %s AND %s >= %s AND %s < %s AND %s > %s AND %s <= %s",
                        TABLE_NAME,
                        COLUMN_DATE,
                        date,
                        COLUMN_START,
                        hs,
                        COLUMN_START,
                        he,
                        COLUMN_END,
                        hs,
                        COLUMN_END,
                        he
                );
        Cursor cursor = db.rawQuery(SELECT_QUERY, null);
        cursor.moveToFirst();
        if (cursor.getCount() != 0) {
            if (cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)).contains(","))
            {
                for (int i =0; i < cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)).split(",").length;i++)
                {
                    r = cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)).split(", ")[i];
                    freeRoom.remove(r);
                }
            }
            else {
                freeRoom.remove(cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)));
            }
            while (cursor.moveToNext())
            {
                if (cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)).contains(","))
                {
                    for (int i =0; i < cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)).split(",").length;i++)
                    {
                        r = cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)).split(", ")[i];
                        freeRoom.remove(r);
                    }
                }
                else {
                    freeRoom.remove(cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION)));
                }
            }
        }
        return freeRoom;
    }

    public void deleteall()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_NAME);
    }

    public int count()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT count(*) FROM "+ TABLE_NAME,null);
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public Course cursorToCourse(Cursor cursor)
    {
        String ue = cursor.getString(cursor.getColumnIndex(COLUMN_UE));
        int hs = cursor.getInt(cursor.getColumnIndex(COLUMN_START));
        int he = cursor.getInt(cursor.getColumnIndex(COLUMN_END));
        int da = cursor.getInt(cursor.getColumnIndex(COLUMN_DATE));
        String location = cursor.getString(cursor.getColumnIndex(COLUMN_LOCATION));
        String type = cursor.getString(cursor.getColumnIndex(COLUMN_TYPE));
        String enseignant = cursor.getString(cursor.getColumnIndex(COLUMN_ENSEIGNANT));
        String group = cursor.getString(cursor.getColumnIndex(COLUMN_GROUPE));
        String gpref = cursor.getString(cursor.getColumnIndex(COLUMN_GPREF));
        Course course = new Course (da,ue, hs,he,location,enseignant,group,type,gpref);
        return course;
    }



}