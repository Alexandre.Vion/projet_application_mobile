package com.example.uapv1705388.emploie_du_temps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    public static final String PREFS = "Promo&Group";
    CourseDbHelper dbh;

    private ArrayList<String> starttimes = new ArrayList<String>();
    private ArrayList<String> endtimes = new ArrayList<String>();
    private ArrayList<String> courses = new ArrayList<String>();
    private ArrayList<String> dates = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setSupportActionBar((Toolbar)findViewById(R.id.toolbar));

        dbh = new CourseDbHelper(getApplicationContext());
        Runnable run = new Runnable() {
            @Override
            public void run() {
                initViewParameter();
                initRecyclerView();
            }
        };
        new UpdateCalendar(dbh,run).execute();
        if(dbh.count()!=0)
        {
            initViewParameter();
            initRecyclerView();

        }



        SharedPreferences settings = getSharedPreferences(PREFS, 0);
        //settings.edit().clear().apply();
        String prefcurriculum = settings.getString("curriculum",null);
        String prefclass = settings.getString("class",null);
        String prefgroup = settings.getString("group",null);
        if(prefcurriculum == null || prefclass == null || prefgroup == null){
            Intent intent =  new Intent(HomeActivity.this,NoSharedPrefHomeActivity.class);
            intent.putExtra("homestate","finish");
            startActivity(intent);
            finish();
        }
        getSupportActionBar().setTitle(prefcurriculum+" "+prefclass+" "+prefgroup);
        Log.d("app", "main group "+prefcurriculum+" "+prefclass+" "+prefgroup);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {

            return true;
        }
        else if(id == R.id.schedule)
        {
            Intent intent =  new Intent(HomeActivity.this,AllCourseActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.evaluation)
        {
            Intent intent =  new Intent(HomeActivity.this,NextEvalListActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.free_room)
        {
		Intent intent =  new Intent(HomeActivity.this, FreeRoomActivity.class);
		startActivity(intent);
            return true;
        }
        else if(id == R.id.option)
        {
            Intent intent =  new Intent(HomeActivity.this,NoSharedPrefHomeActivity.class);
            intent.putExtra("homestate","unfinish");
            startActivityForResult(intent,1);
            return true;
        }
	    else if(id == R.id.ue)
        {
             Intent intent =  new Intent(HomeActivity.this, UEActivity.class);
             startActivity(intent);
             return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == HomeActivity.RESULT_OK) {

                SharedPreferences settings = getSharedPreferences(PREFS, 0);
                String prefcurriculum = settings.getString("curriculum", null);
                String prefclass = settings.getString("class", null);
                String prefgroup = settings.getString("group", null);
                getSupportActionBar().setTitle(prefcurriculum + " " + prefclass + " " + prefgroup);
            }
        }
    }

    private void initViewParameter()
    {
        starttimes.clear();
        endtimes.clear();
        courses.clear();
        dates.clear();

        ArrayList<Course> cCourse = dbh.getNextCourse();

        for(int i =0; i<cCourse.size();i++)
        {

            starttimes.add(cCourse.get(i).startTimeToTxt());
            endtimes.add(cCourse.get(i).endTimeToTxt());
            courses.add(cCourse.get(i).courseToTxt());
            dates.add(cCourse.get(i).dateToTxT());
        }
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        RecyclerViewAdapterCourse adapter = new RecyclerViewAdapterCourse(starttimes,endtimes,courses,dates,this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}
