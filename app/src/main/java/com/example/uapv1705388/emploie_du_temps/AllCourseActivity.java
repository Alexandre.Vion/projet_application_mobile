package com.example.uapv1705388.emploie_du_temps;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

public class AllCourseActivity extends AppCompatActivity {

    private ArrayList<String> starttimes = new ArrayList<String>();
    private ArrayList<String> endtimes = new ArrayList<String>();
    private ArrayList<String> courses = new ArrayList<String>();
    private ArrayList<String> dates = new ArrayList<String>();
    Button opencalendar;
    TextView date;
    Calendar c;
    DatePickerDialog dpd;
    CourseDbHelper dbh;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allcourseday);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("");
        dbh = new CourseDbHelper(getApplicationContext());

        opencalendar = (Button) findViewById(R.id.ocalendar);
        date = (TextView)findViewById(R.id.allcoursedate);

        c=Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH)+1;
        int year = c.get(Calendar.YEAR);
        date.setText(day+"/"+month+"/"+year);

        initViewParameter();
        initRecyclerView();

        opencalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);



                dpd = new DatePickerDialog(AllCourseActivity.this, new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {

                        c.set(Calendar.DAY_OF_MONTH,day);
                        c.set(Calendar.MONTH,month);
                        c.set(Calendar.YEAR,year);

                        initViewParameter();
                        initRecyclerView();

                        month +=1;


                        date.setText(day+"/"+month+"/"+year);
                    }
                },year,month,day);
                dpd.show();

                Log.d("app",c.getTime().toString());
                Log.d("app",day+"/"+month+"/"+year);




            }
        });




    }

    private void initViewParameter()
    {
        starttimes.clear();
        endtimes.clear();
        courses.clear();
        dates.clear();

        ArrayList<Course> cCourse = dbh.getAllCourDay(getDate());


        for(int i =0; i<cCourse.size();i++)
        {
            starttimes.add(cCourse.get(i).startTimeToTxt());
            endtimes.add(cCourse.get(i).endTimeToTxt());
            courses.add(cCourse.get(i).courseToTxt());
            dates.add(cCourse.get(i).dateToTxT());
        }
    }

    private void initRecyclerView(){
        RecyclerView recyclerView = findViewById(R.id.recycler_view_ac);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(starttimes,endtimes,courses,this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    private int getDate()
    {
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH)+1;
        int year = c.get(Calendar.YEAR);

        return year*10000+(month)*100+day;
    }

}
