package com.example.uapv1705388.emploie_du_temps;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class FreeRoomActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public static final String PREFS = "Promo&Group";

    Calendar c;
    DatePickerDialog dpd;
    int date = 0;
    String hStart;
    String durationt;
    CourseDbHelper dbh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_freeroom);

        dbh = new CourseDbHelper(getApplicationContext());
        SharedPreferences settings = getSharedPreferences(PREFS, 0);
        //settings.edit().clear().apply();
        String prefcurriculum = settings.getString("curriculum",null);
        String prefclass = settings.getString("class",null);
        String prefgroup = settings.getString("group",null);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("");
        Log.d("app", "main group "+prefcurriculum+" "+prefclass+" "+prefgroup);

        Button datepicker = (Button) findViewById(R.id.datepicker);
        Button search = (Button) findViewById(R.id.search);
        final Spinner startTime = (Spinner) findViewById(R.id.startTime);
        final Spinner duration = (Spinner) findViewById(R.id.duration);
        final TextView free = (TextView) findViewById(R.id.free);
        final TextView datepick = (TextView) findViewById(R.id.datepick);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.starth, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        startTime.setAdapter(adapter);
        startTime.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,R.array.durationt, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        duration.setAdapter(adapter2);
        duration.setOnItemSelectedListener(this);
        c=Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH)+1;
        int year = c.get(Calendar.YEAR);


        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);



                dpd = new DatePickerDialog(FreeRoomActivity.this, new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {

                        c.set(Calendar.DAY_OF_MONTH,day);
                        c.set(Calendar.MONTH,month);
                        c.set(Calendar.YEAR,year);
                        String d ;
                        if (month+1 < 10)
                        {
                            if (day < 10)
                            {
                                d = String.valueOf(year) + "0" + String.valueOf(month+1) + "0" + String.valueOf(day);
                            }
                            else
                            {
                                d = String.valueOf(year) + "0" + String.valueOf(month+1) + String.valueOf(day);
                            }
                        }
                        else {
                            d = String.valueOf(year) + String.valueOf(month+1) + String.valueOf(day);
                        }
                        date = Integer.valueOf(d);


                        month +=1;


                        datepick.setText(day+"/"+month+"/"+year);
                    }
                },year,month,day);
                dpd.show();

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (date != 0 && hStart.contains("h") && durationt.contains("h")) {
                    int hs = Integer.valueOf(hStart.replace("h", "")) * 100;
                    int d = Integer.valueOf(durationt.replace("h", "")) * 100;
                    int he = hs + d;
                    Log.d("date", String.valueOf(hs));
                    Log.d("date", String.valueOf(he));
                    ArrayList<String> freeroom = dbh.getFreeRoom(date, hs, he);
                    String f = "";
                    for (int i = 0; i < freeroom.size(); i++) {
                        f += freeroom.get(i) + "\n";
                    }
                    free.setText(f);
                } else {
                    Toast.makeText(getApplicationContext(),"Sélectionner les informations demander",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.home) {

            Intent intent =  new Intent(FreeRoomActivity.this, HomeActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.schedule)
        {
            return true;
        }
        else if(id == R.id.evaluation)
        {
            return true;
        }
        else if(id == R.id.free_room)
        {
            return true;
        }
        else if(id == R.id.option)
        {
            Intent intent =  new Intent(FreeRoomActivity.this, NoSharedPrefHomeActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()){
            case R.id.startTime :
                hStart = parent.getItemAtPosition(position).toString();
                parent.setSelection(position);
            case R.id.duration :
                durationt = parent.getItemAtPosition(position).toString();
                parent.setSelection(position);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
