package com.example.uapv1705388.emploie_du_temps;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ApiReader
{
    CourseDbHelper dbh;

    public ApiReader(CourseDbHelper dbh)
    {
        this.dbh = dbh;
    }

    public void read(InputStream in)
    {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in,"UTF-8"));
            String line = br .readLine();
            while(line != null)
            {
                if(line.equals("BEGIN:VEVENT"))
                {
                    Boolean cancel = false;
                    int startdate = 0;
                    int starthours = 0;
                    int enddate = 0;
                    int endhours = 0;
                    String ue = "";
                    String teacher = "";
                    String group = "";
                    String location = "";
                    String type = "";

                    while(!line.equals("END:VEVENT"))
                    {
                        line = br.readLine();
                        if(line.contains("Annulation :"))
                        {
                            cancel = true;
                            break;
                        }
                        else if(line.contains("DTSTART:"))
                        {
                            //Log.d("read",line.substring(8));
                            String[] cds = line.substring(8).split("T");
                            startdate = Integer.parseInt(cds[0]);
                            cds[1] = cds[1].replace("Z","");
                            starthours = Integer.parseInt(cds[1]);
                        }
                        else if(line.contains("DTEND:"))
                        {
                            //Log.d("read",line.substring(6));
                            String[] cde = line.substring(6).split("T");
                            enddate = Integer.parseInt(cde[0]);
                            cde[1] = cde[1].replace("Z","");
                            endhours = Integer.parseInt(cde[1]);
                        }
                        else if(line.contains("DESCRIPTION;"))
                        {
                            //Log.d("read",line.substring(24));
                            String desc = line.substring(24);
                            desc = desc.replace("\\n", "@");
                            desc = desc.replace("\\","");
                            desc = desc.replace("\'"," ");
                            String[] desctab = desc.split("@");
                            for (int i = 0; i < desctab.length; i++) {
                                if (desctab[i].contains("Matière :")) {
                                    //Log.d("read",desctab[i].substring(10));
                                    ue = desctab[i].substring(10);
                                } else if (desctab[i].contains("Enseignant :")) {
                                    //Log.d("read",desctab[i].substring(13));
                                    teacher = desctab[i].substring(13);
                                } else if (desctab[i].contains("Promotions :")) {
                                    //Log.d("read",desctab[i].substring(12));
                                    group = desctab[i].substring(12);}
                                else if (desctab[i].contains("Promotion :")) {
                                    //Log.d("read",desctab[i].substring(11));
                                    group = desctab[i].substring(11);
                                } else if (desctab[i].contains("Salle :")) {
                                    ///Log.d("read",desctab[i].substring(8));
                                    location = desctab[i].substring(8);
                                } else if (desctab[i].contains("Salles :")) {
                                    ///Log.d("read",desctab[i].substring(9));
                                    location = desctab[i].substring(9);
                                } else if (desctab[i].contains("Type :")) {
                                    //Log.d("read",desctab[i].substring(7));
                                    type = desctab[i].substring(7);
                                } else if (desctab[i].contains("TD :")) {
                                    //Log.d("read",desctab[i].substring(5));
                                    group+= desctab[i].substring(5);
                                }

                            }
                        }

                    }
                    if(!cancel)
                    {
                        Course tmp = new Course(startdate,ue,Course.timezone(starthours,startdate),Course.timezone(endhours,startdate),location,teacher,group,type,"2-L3IN");
                        dbh.addCourse(tmp);
                    }
                }
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
