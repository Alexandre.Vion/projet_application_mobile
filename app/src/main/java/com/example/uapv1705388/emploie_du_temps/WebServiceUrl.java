package com.example.uapv1705388.emploie_du_temps;

import android.net.Uri;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    //%TODO choose url from shared pref

    //http://edt-api.univ-avignon.fr/app.php/api/exportAgenda/diplome/2-L3IN


    private static final String HOST = "edt-api.univ-avignon.fr";
    private static final String PATH_1 = "app.php";
    private static final String PATH_2 = "api";
    private static final String PATH_3 = "exportAgenda";
    private static final String PATH_4 = "diplome";
    private static final String PATH_5 = "2-L3IN";

    public static URL build() throws MalformedURLException {

        String group = "2-L3IN";


        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1)
                .appendPath(PATH_2)
                .appendPath(PATH_3)
                .appendPath(PATH_4)
                .appendPath(PATH_5);
        Log.d("url",builder.build().toString());
        URL url = new URL(builder.build().toString());
        return url;
    }
}
