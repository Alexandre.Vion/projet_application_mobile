package com.example.uapv1705388.emploie_du_temps;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class UpdateCalendar extends AsyncTask<Void,Void,String>{

    CourseDbHelper dbh;
    Runnable run;

    public UpdateCalendar(CourseDbHelper dbh,Runnable run)
    {
        this.dbh = dbh;
        this.run =run;
    }

    @Override
    protected String doInBackground(Void... v) {

        URL url = null;
        try {
            url = WebServiceUrl.build();
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            Log.d("app","url");
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            Log.d("app","getinputstream");
            dbh.deleteall();
            Log.d("app",Integer.toString(dbh.count()));
            new ApiReader(dbh).read(in);
            Log.d("app",Integer.toString(dbh.count()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        run.run();
        super.onPostExecute(s);
    }
}
