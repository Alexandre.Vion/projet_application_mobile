package com.example.uapv1705388.emploie_du_temps;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerViewAdapterCourse extends RecyclerView.Adapter<RecyclerViewAdapterCourse.ViewHolder>
{
    private ArrayList<String> starttimes = new ArrayList<String>();
    private ArrayList<String> endtimes = new ArrayList<String>();
    private ArrayList<String> courses = new ArrayList<String>();
    private ArrayList<String> dates = new ArrayList<String>();
    private Context context;


    public RecyclerViewAdapterCourse(ArrayList<String> starttimes, ArrayList<String> endtimes, ArrayList<String> courses,ArrayList<String> dates, Context context) {
        this.starttimes = starttimes;
        this.endtimes = endtimes;
        this.dates=dates;
        this.courses = courses;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_listcourse,viewGroup,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder (@NonNull ViewHolder ViewHolder, int i) {
        ViewHolder.starttime.setText(starttimes.get(i));
        ViewHolder.endtime.setText(endtimes.get(i));
        ViewHolder.date.setText(dates.get(i));
        ViewHolder.course.setText(courses.get(i));
        if(i%2 == 1)
        {
            ViewHolder.parentlayout.setBackgroundResource(R.color.courseLightBackground);
        }
    }

    @Override
    public int getItemCount() {
        return starttimes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView starttime;
        TextView endtime;
        TextView course;
        TextView date;
        ConstraintLayout parentlayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.starttime = itemView.findViewById(R.id.starttime);
            this.endtime = itemView.findViewById(R.id.endtime);
            this.course = itemView.findViewById(R.id.course);
            this.date = itemView.findViewById(R.id.date);
            this.parentlayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
