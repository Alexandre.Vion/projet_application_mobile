package com.example.uapv1705388.emploie_du_temps;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class Course implements Parcelable{
    public static final String TAG = Course.class.getSimpleName();

    public int id;
    public String ue;
    public int hstart;
    public int hend;
    public String teacher;
    public String group;
    public String location;
    public int date;
    public String type;
    public String grouppref;

    public Course() { }

    public Course(int date,String ue, int hstart, int hend, String location, String teacher, String group,String type,String grouppref) {
        this.ue = ue;
        this.hstart = hstart;
        this.hend = hend;
        this.location = location;
        this.teacher = teacher;
        this.group = group;
        this.date = date;
        this.type = type;
        this.grouppref = grouppref;
    }

    protected Course(Parcel in) {
        ue = in.readString();
        hstart = in.readInt();
        hend = in.readInt();
        location = in.readString();
        teacher = in.readString();
        group = in.readString();
        date = in.readInt();
        type = in.readString();
        grouppref = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(ue);
        dest.writeInt(hstart);
        dest.writeInt(hend);
        dest.writeString(location);
        dest.writeString(teacher);
        dest.writeString(group);
        dest.writeInt(date);
        dest.writeString(type);
        dest.writeString(grouppref);

    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", ue='" + ue + '\'' +
                ", hstart=" + hstart +
                ", hend=" + hend +
                ", teacher='" + teacher + '\'' +
                ", group='" + group + '\'' +
                ", location='" + location + '\'' +
                ", date=" + date +
                ", type='" + type + '\'' +
                ", grouppref='"+grouppref+'\''+
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Course> CREATOR = new Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel in) {
            return new Course(in);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };

    public static int timezone(int hour , int date)
    {
        if(date % 10000 > 331 && date % 10000 < 1027)
        {
            return hour + 20000;
        }
        return hour + 10000;
    }

    public String startTimeToTxt()
    {
        int hour = this.hstart /10000;
        int minute = (this.hstart % 10000) / 100;
        return twoDigitNumber(hour)+"H"+twoDigitNumber(minute);
    }

    public  String endTimeToTxt() {
        int hour = this.hend / 10000;
        int minute = (this.hend % 10000) / 100;
        return twoDigitNumber(hour) + "H" + twoDigitNumber(minute);
    }

    public String dateToTxT()
    {
        int year = this.date /10000;
        int month = (this.date % 10000) /100;
        int day = this.date % 100;
        return twoDigitNumber(day)+"/"+twoDigitNumber(month)+"/"+Integer.toString(year);
    }

    public  String courseToTxt()
    {
        return "Ue: "+this.ue+"\n"+"Enseignant: "+this.teacher+"\n"+"Groupe: "+this.group+"\n"+"Salle: "+this.location+"\n"+"Type: "+this.type;
    }

    private String twoDigitNumber(int number)
    {
        if(number<10)
        {
            return "0"+Integer.toString(number);
        }
        return Integer.toString(number);
    }
}

